package com.scion.scionwheeloffate;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CharacterTemplateAdapter extends BaseAdapter {
	private Context myContext;
	private LayoutInflater inflater;
	public CharacterTemplateList templates; 
	
	public CharacterTemplateAdapter(Activity c, CharacterTemplateList l) {
		myContext = c;
		inflater = LayoutInflater.from(c);
		templates = l;
		l.attachAdapter(this);
	}
	
	public void setTemplateList(CharacterTemplateList ctl)
	{
		templates = ctl;
	}
	
	@Override
	public int getCount() {
		
		return templates.templateList.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return templates.templateList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		
		return arg0;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView tv;
		Log.v("CTLA", "getDropDownView=" + position);
		if (convertView == null) {  // if it's not recycled, initialize some attributes
            tv = new TextView(myContext);
        } else {
            tv = (TextView) convertView;
        }
		tv.setText(templates.templateList.get(position).name);
		tv.setTextColor(Color.rgb(0, 0, 0));
		tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
		return tv;
	}
	
	@Override
	public View getView(int arg0, View convertView, ViewGroup parent) {
		View rl;
		TextView tv;
		Log.v("CTLA", "getView=" + arg0);
		if (convertView == null) {  // if it's not recycled, initialize some attributes
            rl = inflater.inflate(R.layout.charentry,null);
        } else {
            rl = convertView;
        }
		
		CharacterTemplate ct;
		
		ct = templates.templateList.get(arg0);
		
		tv = (TextView)rl.findViewById(R.id.leName);
		tv.setText(ct.name);
		
		tv = (TextView)rl.findViewById(R.id.leIni);
		tv.setText(Integer.toString(ct.witsAwareness));
		
		tv = (TextView)rl.findViewById(R.id.leEw);
		tv.setText(Integer.toString(ct.epicWits));
		
		tv = (TextView)rl.findViewById(R.id.leDv);
		tv.setText(Integer.toString(ct.DV));
		
		tv = (TextView)rl.findViewById(R.id.leLegend);
		tv.setText(ct.bLegend ? "Legend" : "Mundane");
		
		tv = (TextView)rl.findViewById(R.id.leEnemy);
		tv.setText(ct.bEnemy ? "Hostile" : "Friendly");
		
		
		
		//tv.setTextColor(Color.rgb(255, 255, 255));
		//tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
		return rl;
	}

}
