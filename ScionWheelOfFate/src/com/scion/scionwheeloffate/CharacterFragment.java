package com.scion.scionwheeloffate;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

public class CharacterFragment extends Fragment {
	
	
	private CharacterTemplateList templateList = new CharacterTemplateList();
	public CharacterTemplateAdapter templateAdapter;
	private ListView templateListView;
	private Button addButton;
	
	
	private EditText chName;
	private EditText chIni;
	private EditText chEW;
	private EditText chDV;
	private CheckBox chLegend;
	private CheckBox chEnemy;
	
	private OnClickListener addCharListener = new OnClickListener() {
	    public void onClick(View v) {
	    	
	    	if(chName.getText().length()==0) return;
	    	
	    	
	    	Log.v("ChFr","++"+chIni.getText().toString()+"++");
	    	
	    	Log.v("ChFr",Integer.getInteger("23", 5).toString() );
	    	
	    	
	    	int ini;
	    	int ew;
	    	int dv;
	    	
	    	try { ini = Integer.parseInt(chIni.getText().toString()); } catch (NumberFormatException nfe) { ini=5;};
	    	try { ew = Integer.parseInt(chEW.getText().toString()); } catch (NumberFormatException nfe) { ew=0;};
	    	try { dv = Integer.parseInt(chDV.getText().toString()); } catch (NumberFormatException nfe) { dv=5;};
	    	
	    	
	    	templateList.templateList.add(new CharacterTemplate(chName.getText().toString(),
	     		 ini,ew,dv,
	    		 chLegend.isChecked(),chEnemy.isChecked()));
	    	templateList.notifyAdapters();
	      
	      
	    	chName.setText("");
	    	chIni.setText("");
	    	chEW.setText("");
	    	chDV.setText("");
	    	chLegend.setChecked(true);
	      	chEnemy.setChecked(false);
	      
	    }
	};

	
	public void setAdapter(CharacterTemplateAdapter a)
	{
		templateAdapter = a;
		templateListView.setAdapter(a);
	}
	
	public void setTemplateList(CharacterTemplateList ctl)
	{
		templateList = ctl;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		
		View fragView = inflater.inflate(R.layout.characters,container,false);
		
		templateListView = (ListView) fragView.findViewById(R.id.templateList);
		
		
		addButton = (Button) fragView.findViewById(R.id.CharAdd);
		addButton.setOnClickListener(addCharListener);
		
		chName = (EditText)fragView.findViewById(R.id.leName);
		chIni = (EditText)fragView.findViewById(R.id.CharIni);
		chEW = (EditText)fragView.findViewById(R.id.CharEpicWits);
		chDV = (EditText)fragView.findViewById(R.id.CharDV);
		chLegend = (CheckBox)fragView.findViewById(R.id.CharLegend);
		chEnemy = (CheckBox)fragView.findViewById(R.id.CharEnemy);
		
		return fragView;
	}
	
	

}
