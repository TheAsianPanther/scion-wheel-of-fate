package com.scion.scionwheeloffate;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.widget.BaseAdapter;

public class CharacterTemplateList {
	public Vector<CharacterTemplate> templateList = new Vector<CharacterTemplate>();
	public Context myContext;
	private String FILENAME = "char_templates";
	
	private Vector<BaseAdapter> attachedAdapters = new Vector<BaseAdapter>();
	
	public void attachAdapter(BaseAdapter a){
		attachedAdapters.add(a);
	}
	
	public void notifyAdapters(){
		for(Iterator<BaseAdapter> i = attachedAdapters.iterator();i.hasNext();)
		{
			i.next().notifyDataSetChanged();
		}
	}
	
	public void fromJSONString(String jsonstring) {
		
		try {
			JSONArray jarray = new JSONArray(jsonstring);
			
			for(int i=0;i<jarray.length();i++) 
				templateList.add(new CharacterTemplate(jarray.getJSONObject(i)));
		} catch (JSONException jse)
		{
			
		}
		
	}
	
	public String toJSONString() {
		
		
		JSONArray jarray = new JSONArray();
		
		for(int i=0;i<jarray.length();i++) 
			jarray.put( templateList.get(i).toJSON());
		return jarray.toString();
		
		
	}
	
	public void save()
	{
		
		
		try
		{
			FileOutputStream fos = myContext.openFileOutput(FILENAME, Context.MODE_PRIVATE);
			fos.write(toJSONString().getBytes());
			fos.close();
		} catch (IOException ioe)
		{
		
		}
	}
	
	
	public void load()
	{
		try
		{
			byte[] buffer = new byte[2048];
			FileInputStream fis = myContext.openFileInput(FILENAME);
			
			fis.read(buffer);
			fromJSONString(buffer.toString());
			fis.close();
		} catch (IOException ioe)
		{
		
		}
	}

}
