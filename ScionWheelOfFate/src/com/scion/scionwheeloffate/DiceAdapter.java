package com.scion.scionwheeloffate;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DiceAdapter extends BaseAdapter {

	private int[] dices;
	private int nDices=0;
	private boolean bLegend=true;
	private Context myContext;
	public DiceAdapter(Context c) {
		myContext = c;
	}
	
	@Override
	public int getCount() {
		//Log.v("DiceAdapter", "getCount=" + nDices);
		return nDices;
	}

	@Override
	public Object getItem(int arg0) {
		
		if(dices == null) return 0;
		else return dices[arg0];
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setDices(int[] newDices, int numDices, boolean isLegend) {
		dices=newDices;
		nDices=numDices;
		bLegend=isLegend;
		//Log.v("DiceAdapter", "numDices=" + numDices);
		notifyDataSetChanged();
	}
	
	@Override
	public View getView(int arg0, View convertView, ViewGroup parent) {
		TextView tv;
		//Log.v("DiceAdapter", "getView=" + arg0);
		if (convertView == null) {  // if it's not recycled, initialize some attributes
            tv = new TextView(myContext);
        } else {
            tv = (TextView) convertView;
        }
		tv.setText(String.valueOf(dices[arg0]));
		tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24);
		tv.setGravity(Gravity.CENTER);
		if(dices[arg0]==10 && bLegend) tv.setTextColor(Color.rgb(255, 220, 100));
		else if(dices[arg0]>=7) tv.setTextColor(Color.rgb(50, 255, 50));
		else if(dices[arg0]==1) tv.setTextColor(Color.rgb(250, 40, 40));
		else tv.setTextColor(Color.rgb(150, 150, 150));
		return tv;
		/*Log.v("DiceAdapter", "getView=" + arg0);
		tv = new TextView(myContext);
		tv.setText("7");
		tv.setGravity(Gravity.CENTER);
		tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24);
		tv.setTextColor(Color.rgb(255, 220, 100));
		return tv;*/
	}

}
