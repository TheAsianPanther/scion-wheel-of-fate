package com.scion.scionwheeloffate;

import java.util.Random;
import java.util.TimerTask;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DicerFragment extends Fragment {
	private int[] results;
	private int nDice = 0;
	private int nSuccesses = 0;
	private int nBotches = 0;
	private Random myRandomizer;
	
	private DiceAdapter diceAdapter;
	private TextView diceMessage;
	private GridView gridview;
	private CheckBox legendBox;
	private View lastDiceClicked;
	
	private Handler mHandler = new Handler();

	
	class UpdateTimeTask extends TimerTask {
		   public void run() {
			   gridview.setNumColumns( (int)Math.ceil(Math.sqrt(nDice)) );
		       diceAdapter.setDices(results,nDice,legendBox.isChecked());
		       

		   }
		};
	
	private UpdateTimeTask mu = new UpdateTimeTask();
	
	
	public void refreshDiceDisplay(View v)
	{
		TextView tv = (TextView) v;
        if(tv != null)
        {
      	  nDice = Integer.parseInt(tv.getText().toString());
      	  nSuccesses = 0;
      	  nBotches = 0;
      	  for(int i=0;i<nDice;i++)
      	  {
      		  results[i]=myRandomizer.nextInt(10)+1;
      		  if(results[i]==10 && legendBox.isChecked()) nSuccesses+=2;
      		  else if(results[i]>=7) nSuccesses++;
      		  if(results[i]==1) nBotches++;
      	  }
      	  
      	  
        }
        diceMessage.setText(String.format("Successes:%d \n Botches:%d", nSuccesses, nBotches));
        diceAdapter.setDices(results, 0,true);
        mHandler.removeCallbacks(mu);
        mHandler.postDelayed(mu, 100);
        //gridview.setNumColumns( (int)Math.ceil(Math.sqrt(nDice)) );
	    //diceAdapter.setDices(results,nDice);
	}
	
    private OnClickListener diceClickListener = new OnClickListener() {
        public void onClick(View v) 
        {
        	lastDiceClicked=v;
        	refreshDiceDisplay(v);
        }
    };

    private OnClickListener LegendClickListener = new OnClickListener() {
        public void onClick(View v) 
        {
        	nSuccesses = 0;
        	nBotches = 0;
			for(int i=0;i<nDice;i++)
			{
				if(results[i]==10 && legendBox.isChecked()) nSuccesses+=2;
				else if(results[i]>=7) nSuccesses++;
				if(results[i]==1) nBotches++;
			}
			diceMessage.setText(String.format("Successes:%d \n Botches:%d", nSuccesses, nBotches));
			diceAdapter.setDices(results,nDice,legendBox.isChecked());
        }
    };
    
	@Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
		        Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		
		results = new int[30];
		myRandomizer = new Random();
		
		View fragView = inflater.inflate(R.layout.dicer,container,false);
		LinearLayout ll = (LinearLayout) fragView.findViewById(R.id.DiceBarLayout);
		diceMessage = (TextView) fragView.findViewById(R.id.DiceMessage);
		
		legendBox = (CheckBox) fragView.findViewById(R.id.checkLegend);
		legendBox.setOnClickListener(LegendClickListener);
		diceAdapter = new DiceAdapter(getActivity());
		
		gridview = (GridView) fragView.findViewById(R.id.diceResultBox);
		gridview.setAdapter(diceAdapter);
		
		TextView textView;
		
		int i;
		for(i=0;i<30;i++)
		{
			textView = new TextView(getActivity());
			textView.setText(String.valueOf(i+1));
			textView.setPadding(0, 0, 32, 0);
			textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 32);
			textView.setTextColor(Color.rgb(255, 255, 255));
			textView.setClickable(true);
			textView.setOnClickListener(diceClickListener);
			ll.addView(textView);
		}
		
		
		return fragView;
		
	}

}
