package com.scion.scionwheeloffate;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity implements ActionBar.TabListener{

	Fragment DicerFragment;
	CharacterFragment CharFragment;
	CombatFragment combatFragment;
	
	
	
	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		if(arg0.getText() == "Dicer")
		{
			if(CharFragment != null)
				arg1.hide(CharFragment);
			if(combatFragment != null)
				arg1.hide(combatFragment);
		}
		else if(arg0.getText() == "Characters")
		{
			if(DicerFragment != null)
				arg1.hide(DicerFragment);
			if(combatFragment != null)
				arg1.hide(combatFragment);
		}
		else
		{
			if(CharFragment != null)
				arg1.hide(CharFragment);
			if(DicerFragment != null)
				arg1.hide(DicerFragment);
		}
		
	}

	@Override
	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		if(arg0.getText() == "Dicer")
		{
			if(DicerFragment != null)
				arg1.show(DicerFragment);
		}
		else if(arg0.getText() == "Characters")
		{
			if(CharFragment != null)
				arg1.show(CharFragment);
		}
		else
		{
			if(combatFragment != null)
				arg1.show(combatFragment);
		}
		
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		
		
	}

	

	
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final ActionBar bar = getActionBar();
		 bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	        bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);

	        bar.addTab(bar.newTab()
	                .setText("Dicer").setTabListener(this)
	                );
	        bar.addTab(bar.newTab()
	                .setText("Combat Wheel").setTabListener(this)
	                );
	        bar.addTab(bar.newTab()
	                .setText("Characters").setTabListener(this)
	                );
		
		
	    CharacterTemplateList ctl = new CharacterTemplateList();
	    CharacterTemplateAdapter cta = new CharacterTemplateAdapter(this, ctl);    
	    CharacterTemplateSpinnerAdapter ctsa = new CharacterTemplateSpinnerAdapter(this, ctl);
		DicerFragment = getFragmentManager().findFragmentById(R.id.fragment1);
		
		
		
		
		CharFragment = (CharacterFragment)getFragmentManager().findFragmentById(R.id.fragment3);
		combatFragment = (CombatFragment) getFragmentManager().findFragmentById(R.id.fragment2);
		
		CharFragment.setAdapter(cta);
		CharFragment.setTemplateList(ctl);
		combatFragment.setAdapter(ctsa);
		
		getFragmentManager().beginTransaction().hide(CharFragment).hide(combatFragment).commit();
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
