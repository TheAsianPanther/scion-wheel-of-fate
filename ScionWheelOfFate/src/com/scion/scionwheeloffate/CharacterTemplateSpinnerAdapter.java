package com.scion.scionwheeloffate;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CharacterTemplateSpinnerAdapter extends BaseAdapter {
	private Context myContext;
	public CharacterTemplateList templates; 
	
	public CharacterTemplateSpinnerAdapter(Activity c, CharacterTemplateList l) {
		myContext = c;
		templates = l;
		l.attachAdapter(this);
	}
	
	public void setTemplateList(CharacterTemplateList ctl)
	{
		templates = ctl;
	}
	
	@Override
	public int getCount() {
		
		return templates.templateList.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return templates.templateList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		
		return arg0;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView tv;
		Log.v("CTLA", "getDropDownView=" + position);
		if (convertView == null) {  // if it's not recycled, initialize some attributes
            tv = new TextView(myContext);
        } else {
            tv = (TextView) convertView;
        }
		tv.setText(templates.templateList.get(position).name);
		tv.setTextColor(Color.rgb(0, 0, 0));
		tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
		return tv;
	}
	
	@Override
	public View getView(int arg0, View convertView, ViewGroup parent) {
		TextView tv;
		//Log.v("DiceAdapter", "getView=" + arg0);
		if (convertView == null) {  // if it's not recycled, initialize some attributes
            tv = new TextView(myContext);
        } else {
            tv = (TextView) convertView;
        }
		
		CharacterTemplate ct;
		
		ct = templates.templateList.get(arg0);
		
		
		tv.setText(ct.name);
		
		
		tv.setTextColor(Color.rgb(255, 255, 255));
		tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
		return tv;
	}

}
