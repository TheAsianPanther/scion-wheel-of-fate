package com.scion.scionwheeloffate;

import org.json.JSONException;
import org.json.JSONObject;

public class CharacterTemplate {
	public String name;
	public int witsAwareness;
	public int epicWits;
	public int DV;
	public boolean bLegend;
	public boolean bEnemy;

	
	public CharacterTemplate(String n, int wa, int ew, int dv, boolean l, boolean e)
	{
		name=n;
		witsAwareness=wa;
		epicWits=ew;
		DV=dv;
		bLegend=l;
		bEnemy=e;
	}
	
	public CharacterTemplate(JSONObject json)
	{
		fromJSON(json);
	}
	
	public void fromJSON(JSONObject json)
	{
		try{
			name = json.getString("Name");
			witsAwareness = json.getInt("WitsAwareness");
			epicWits = json.getInt("EpicWits");
			DV = json.getInt("DV");
			bLegend = json.getBoolean("Legend");
			bEnemy = json.getBoolean("Enemy");
		} catch (JSONException jse)
		{
			
		}
	}
	
	public JSONObject toJSON()
	{
		JSONObject json = new JSONObject();
		try{
			json.put("Name", name);
			json.put("WitsAwareness",witsAwareness);
			json.put("EpicWits",epicWits);
			json.put("DV", DV);
			json.put("Legend", bLegend);
			json.put("Enemy", bEnemy);
		} catch (JSONException jse)
		{
			
		}
		
		return json;
	}
	
}
