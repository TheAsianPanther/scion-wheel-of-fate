package com.scion.scionwheeloffate;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;

public class CombatFragment extends Fragment {
	
	public BaseAdapter templateAdapter;
	private View fragView;
	
	public void setAdapter(BaseAdapter a)
	{
		Spinner sp = (Spinner)fragView.findViewById(R.id.CharChoose);
		sp.setAdapter(a);
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		
		fragView = inflater.inflate(R.layout.combatwheel,container,false);
		
		
		
		return fragView;
	}

}
